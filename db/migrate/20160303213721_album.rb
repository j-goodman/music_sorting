class Album < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :name
      t.integer :band_id
    end

    add_index :albums, :name
  end
end
