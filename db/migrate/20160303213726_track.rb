class Track < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :name
      t.integer :album_id
    end

    add_index :tracks, :name
  end
end
