class Band < ActiveRecord::Migration
  def change
    create_table :bands do |t|
      t.string :name
    end

    add_index :bands, :name
  end
end
