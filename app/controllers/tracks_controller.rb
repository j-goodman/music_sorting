class TracksController < ApplicationController

  def new
    render :new
  end

  def show
    @track = Track.find(params[:id])
    redirect_to band_url(@track.band)
  end

  def create
    @track = Track.new(track_params)
    if @track.save
      redirect_to track_url(@track)
    else
      render :new
    end
  end

  private

  def track_params
    params.require(:track).permit(:name, :album_id)
  end
end
