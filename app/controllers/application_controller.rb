class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user

  def current_user
    found = User.find_by(session_token: session[:session_token])
    return nil unless found
    @user = found
  end

  def logged_in?
    !!current_user
  end

  def login_user!(user)
    user.reset_session_token!
    session[:session_token] = user.session_token
  end

  def logout_user!
    session[:session_token] = nil
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
