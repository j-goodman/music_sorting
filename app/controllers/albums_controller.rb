class AlbumsController < ApplicationController

  def new
    render :new
  end

  def show
    @album = Album.find(params[:id])
    redirect_to band_url(@album.band)
  end

  def create
    @album = Album.new(album_params)
    if @album.save
      redirect_to album_url(@album)
    else
      render :new
    end
  end

  private

  def album_params
    params.require(:album).permit(:name, :band_id)
  end
end
