class User < ActiveRecord::Base
  validates :email, uniqueness: true, presence: true
  validates :password_digest, presence: true
  validates :session_token, uniqueness: { scope: :session_token }

  after_initialize :ensure_session_token

  attr_reader :password

  def self.find_by_credentials(params)
    email, pword = params[:email], params[:password]
    user = User.find_by(email: email)
    user = nil unless user && user.is_password?(pword)
    user
  end

  def self.random_token
    SecureRandom.urlsafe_base64
  end

  def reset_session_token!
    self.session_token = User.random_token
    self.save!
    self.session_token
  end

  def ensure_session_token
    self.session_token = User.random_token unless self.session_token
  end

  def is_password?(pword)
    BCrypt::Password.new(password_digest).is_password?(pword)
  end

  def password=(pword)
    @password = pword
    self.password_digest = BCrypt::Password.create(pword)
  end

end
